# README #
This repository includes the ground-truth dataset collected and labeled for the POISED paper. It includes two files with the following formats:

## groups-of-similar-messages ##
This file includes groups of similar messages. Each line shows a group and is organized as: group_id, group_size, and a list of seed_id-user_id-tweet_id that are comma separated.

## poised-groundtruth-dataset.csv ##
This file includes the labels for every group of similar messages. Every line shows a group_id and its label.

## Cite our work! ##
@inproceedings{poised-ccs14, 
author = author={Nilizadeh, Shirin and Labr{`e}che, Fran{\c{c}}ois and Sedighian, Alireza and Zand, Ali and Fernandez, Jos{'e} and Kruegel, Christopher and Stringhini, Gianluca and Vigna, Giovanni}, 
title={{POISED: Spotting Twitter Spam Off the Beaten Paths}}, 
booktitle = {Proceedings of the 2017 ACM Conference on Computer and Communications Security}, 
series = {CCS '17}, month = nov, year = {2017}, 
location = {Dallas, Texas, USA}, 
publisher = {ACM}, 
address = {New York, NY, USA}
}